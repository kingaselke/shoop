<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180609134712 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE house (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE products_bought (id INT AUTO_INCREMENT NOT NULL, person_id INT DEFAULT NULL, product VARCHAR(255) NOT NULL, price NUMERIC(5, 2) NOT NULL, date DATE NOT NULL, INDEX IDX_829ABDC5217BBB47 (person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_to_buy (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_to_buy_house (product_to_buy_id INT NOT NULL, house_id INT NOT NULL, INDEX IDX_E9F436382892969 (product_to_buy_id), INDEX IDX_E9F436386BB74515 (house_id), PRIMARY KEY(product_to_buy_id, house_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, house_id INT NOT NULL, username VARCHAR(128) NOT NULL, password VARCHAR(4096) NOT NULL, email VARCHAR(255) NOT NULL, INDEX IDX_8D93D6496BB74515 (house_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE products_bought ADD CONSTRAINT FK_829ABDC5217BBB47 FOREIGN KEY (person_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE product_to_buy_house ADD CONSTRAINT FK_E9F436382892969 FOREIGN KEY (product_to_buy_id) REFERENCES product_to_buy (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_to_buy_house ADD CONSTRAINT FK_E9F436386BB74515 FOREIGN KEY (house_id) REFERENCES house (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6496BB74515 FOREIGN KEY (house_id) REFERENCES house (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_to_buy_house DROP FOREIGN KEY FK_E9F436386BB74515');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6496BB74515');
        $this->addSql('ALTER TABLE product_to_buy_house DROP FOREIGN KEY FK_E9F436382892969');
        $this->addSql('ALTER TABLE products_bought DROP FOREIGN KEY FK_829ABDC5217BBB47');
        $this->addSql('DROP TABLE house');
        $this->addSql('DROP TABLE products_bought');
        $this->addSql('DROP TABLE product_to_buy');
        $this->addSql('DROP TABLE product_to_buy_house');
        $this->addSql('DROP TABLE user');
    }
}
