<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class MainController
 * @package App\Controller
 */
class MainController extends Controller
{
    /**
     * @Route("/", name="home_page")
     */
    public function index(Request $request){

        $houseName = $this->getUser()->getHouse()->getName();
        $userId = $this->getUser()->getId();
        return $this->render('homePage.html.twig', array('houseName' => $houseName, 'userId' =>$userId ));
    }

    /**
     * @Route("/login", name="login")
     */

    public function login(Request $request, AuthenticationUtils $authenticationUtils) : Response {
        $errors = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        
        return $this->render('User/login.html.twig', [
            'errors' => $errors,
            'username' => $lastUsername
        ]);

    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout() : Response {}

}