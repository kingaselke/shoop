<?php

namespace App\Controller;
use App\Entity\ProductsBought;
use App\Entity\Roomer;
use App\Entity\User;
use App\Form\RoomerType;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;



class UserController extends Controller
{
    /**
     * @Route("/information", name="information")
     */
    public function getInfoAction(Request $request){
        $houseId = $this->getUser()->getHouse()->getId();

        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findBy([
                'house' => $houseId
            ]);

        return $this->render('information.html.twig', array('users' => $users));


    }

    /**
     * @Route("/settlement/user_{id}/", name="settlement")
     */
    public function settlementAction(Request $request, $id, $month = 'all'){


        $month = $request->query->all();
        $userId = $this->getUser()->getId();
        if ($id != $userId) {
            return $this->redirectToRoute('home_page');
        }

        $houseId = $this->getUser()->getHouse()->getId();

        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findBy([
                'house' => $houseId
            ]);

        foreach ($users as $user) {
            $userId = $user->getId();
            $expense [$userId] = [];
            $expense [$userId] = $this->getDoctrine()
                ->getRepository(ProductsBought::class)
                ->findBy([
                    'person' => $userId,

                ]);

        }

        $price = 0;
        foreach ($expense as $key => $item) {
            foreach ($item as $product) {
                $date = $product->getDate();

                if (!empty($month)) {
                    if ($date->format('m') == $month['month']) {
                    $price += $product->getPrice();
                    }
                } else {
                    $price += $product->getPrice();
                }
            }
            $pricePerPerson [$key] = $price;
            $price = 0;
        }


        return $this->render('settlement.html.twig',[
            'users' => $users,
            'userId' => $this->getUser()->getId(),
            'expense' => $pricePerPerson
        ]);


    }
}