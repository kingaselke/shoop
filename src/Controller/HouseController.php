<?php
/**
 * Created by PhpStorm.
 * User: kina
 * Date: 2018-05-23
 * Time: 19:40
 */
namespace App\Controller;


use App\Entity\House;
use App\Form\HouseType;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HouseController extends Controller
{
    /**
     * @Route("/addHouse", name="add_house")
     */
    public function addHouseAction(Request $request){

        $house = new House();
        $form = $this->createForm(HouseType::class, $house);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                // Save
                $em = $this->getDoctrine()->getManager();
                $em->persist($house);
                $em->flush();

                return $this->redirectToRoute('login');
            } catch (UniqueConstraintViolationException $exception) {
                return $this->render('alert.html.twig', array(
                    'form' => $form->createView()));
            }

        }

        return $this->render('addHouse.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}