<?php

namespace App\Controller;
use App\Entity\ProductsBought;
use App\Entity\ProductToBuy;
use App\Entity\ProductToBuyHouse;
use App\Form\ProductToBuyType;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class ProductController extends Controller
{
    /**
     * @Route("/shoppingList/user_{id}/", name="shopping_list")
     */
    public function shoppingAction(Request $request, $id){

        $userId = $this->getUser()->getId();
        if ($id != $userId) {
            return $this->redirectToRoute('home_page');
        }
        $houseId = $this->getUser()->getHouse()->getId();
        $productToBuy = new ProductToBuy();
        $house = $this->getUser()->getHouse();
        $productToBuy->addHouse($house);
        $form = $this->createForm(ProductToBuyType::class, $productToBuy);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

                $em = $this->getDoctrine()->getManager();
                $em->persist($productToBuy);
                $em->flush();
        }

        $product = $this->getDoctrine()
            ->getRepository(ProductToBuyHouse::class)
            ->findBy(['HouseId' => $houseId]);
        $productNames = [];
        foreach ($product as $item) {

            $productName = $this->getDoctrine()
                ->getRepository(ProductToBuy::class)
                ->findBy(['id' => $item->getProductToBuyId()]);
            $productNames[$productName[0]->getId()] = $productName[0]->getName();

        }

        return $this->render('shoppingList.html.twig', [
            'userId' => $userId,
            'form' => $form->createView(),
            'houseId' => $houseId,
            'productNames' =>$productNames
            ]
        );
    }

        /**
         * @Route("/bought/{id}/", name="bought")
         */
        public function shopAction($id)
        {
            $userId = $this->getUser()->getId();
            $productBought = new ProductsBought();
            $productBought->setPerson($this->getUser());
            $product = $this->getDoctrine()
                ->getRepository(ProductToBuy::class)
                ->find($id);
            $productBought->setProduct($product->getName());
            $productBought->setPrice($_POST['price']);
            $date = new \DateTime();
            $productBought->setDate($date);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($productBought);
            $entityManager->remove($product);
            $entityManager->flush();

            return $this->redirectToRoute('shopping_list', ['id' => $userId]);
        }

}