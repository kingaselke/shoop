<?php
/**
 * Created by PhpStorm.
 * User: kina
 * Date: 2018-05-23
 * Time: 19:40
 */
namespace App\Controller;


use App\Entity\User;
use App\Form\UserType;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RegisterController extends Controller
{
    /**
     * @Route("/registration", name="registration")
     */
    public function registerAction(Request $request){

        $user = new User();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
           // dump($user->getHouse());die();
            try {
                $encoder = $this->get('security.password_encoder');
                $password = $encoder->encodePassword($user, $user->getPassword());
                $user->setPassword($password);


                // Set their role
                $user->setRole('ROLE_USER');

                // Save
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                return $this->redirectToRoute('login');
            } catch (UniqueConstraintViolationException $exception) {
                return $this->render('alert.html.twig', array(
                    'form' => $form->createView()));
            }
        }

        return $this->render('register.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}