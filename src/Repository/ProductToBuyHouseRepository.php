<?php

namespace App\Repository;

use App\Entity\ProductToBuyHouse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductToBuyHouse|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductToBuyHouse|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductToBuyHouse[]    findAll()
 * @method ProductToBuyHouse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductToBuyHouseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductToBuyHouse::class);
    }

//    /**
//     * @return ProductToBuyHouse[] Returns an array of ProductToBuyHouse objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductToBuyHouse
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
