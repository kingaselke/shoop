<?php

namespace App\Repository;

use App\Entity\ProductToBuy;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductToBuy|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductToBuy|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductToBuy[]    findAll()
 * @method ProductToBuy[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductToBuyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductToBuy::class);
    }

//    /**
//     * @return ProductToBuy[] Returns an array of ProductToBuy objects
//     */
////    /*
//    public function findByExampleField($value)
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }
//    */

    /*
    public function findOneBySomeField($value): ?ProductToBuy
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
