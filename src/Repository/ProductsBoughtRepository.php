<?php

namespace App\Repository;

use App\Entity\ProductsBought;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductsBought|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductsBought|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductsBought[]    findAll()
 * @method ProductsBought[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductsBoughtRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductsBought::class);
    }

//    /**
//     * @return ProductsBought[] Returns an array of ProductsBought objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductsBought
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
