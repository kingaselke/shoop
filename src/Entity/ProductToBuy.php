<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductToBuyRepository")
 */
class ProductToBuy
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
    * @ORM\Column(type="string", length=255)
    */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\House", inversedBy="productsToBuy")
     *  @ORM\JoinColumn(name="house_id", referencedColumnName="id")
     */
    private $house;

    public function __construct()
    {
        $this->house = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Collection|House[]
     */
    public function getHouse(): Collection
    {
        return $this->house;
    }

    public function addHouse(House $house): self
    {
        if (!$this->house->contains($house)) {
            $this->house[] = $house;
        }

        return $this;
    }

    public function removeHouse(House $house): self
    {
        if ($this->house->contains($house)) {
            $this->house->removeElement($house);
        }

        return $this;
    }
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
