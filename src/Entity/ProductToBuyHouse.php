<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductToBuyHouseRepository")
 * @ORM\Table(name="product_to_buy_house")
 */
class ProductToBuyHouse
{


    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private $productToBuyId;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private $HouseId;


    public function getProductToBuyId(): ?int
    {
        return $this->productToBuyId;
    }

    public function setProductToBuyId(int $productToBuyId): self
    {
        $this->productToBuyId = $productToBuyId;

        return $this;
    }

    public function getHouseId(): ?int
    {
        return $this->HouseId;
    }

    public function setHouseId(int $HouseId): self
    {
        $this->HouseId = $HouseId;

        return $this;
    }
}
